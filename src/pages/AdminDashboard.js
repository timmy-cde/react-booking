import { useContext, useEffect, useState } from 'react';
import { Table, Button } from 'react-bootstrap'
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AdminDashboard() {
    
    // to validate the user role
    const {user} = useContext(UserContext);

    // Create allCourses state to contain the courses from the database
    const [allCourses, setAllCourses] = useState([]);

    // "fetchData()" wherein we can invoke if there is a certain change with the courses.
    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/all`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setAllCourses(data.map(course => {
                return (
                  <tr key={course._id}>
                    <td>{course._id}</td>
                    <td>{course.name}</td>
                    <td>{course.description}</td>
                    <td>{course.price}</td>
                    <td>{course.slots}</td>
                    <td>{course.isActive ? "Active" : "Inactive"}</td>
                    <td>
                      {course.isActive ? (
                        <Button variant="danger" size="sm" onClick = {() => archive(course._id, course.name)}>
                          Archive
                        </Button>
                      ) : (
                        <>
                          <Button variant="success" size="sm" onClick = {() => unarchive(course._id, course.name)}>
                            Unarchive
                          </Button>
                          <Button variant="dark" size="sm">
                            Edit
                          </Button>
                        </>
                      )}
                    </td>
                  </tr>
                );
            }))
        })
    }

    // Making the course inactive
    const archive = (courseId, courseName) => {
        console.log(courseId);
        console.log(courseName);

        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: false
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data) {
                Swal.fire({
                    title: "Archive Successful!",
                    icon: "success",
                    text: `${courseName} is now inactive.`
                })
                fetchData();
            }
            else {
                Swal.fire({
                    title: "Archive unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                })

            }
        })
    }

    // Making the course active
    const unarchive = (courseId, courseName) => {
        console.log(courseId);
        console.log(courseName);

        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: true
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data) {
                Swal.fire({
                    title: "Unarchive Successful!",
                    icon: "success",
                    text: `${courseName} is now active.`
                })
                fetchData();
            }
            else {
                Swal.fire({
                    title: "Unarchive unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later!"
                })

            }
        })
    }

    // To fetch all courses in the first render of the page
    useEffect(() => {
        // invoke fetchData() to get all courses.
        fetchData();
    }, [])

    return user.isAdmin ? (
      <>
        <div className="mt-5 mb-3 text-center">
          <h1>Admin Dashboard</h1>
          <Button variant="primary" size="lg" className="mx-2">
            Add Course
          </Button>
          <Button variant="success" size="lg" className="mx-2">
            Show Enrollments
          </Button>
        </div>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Course Id</th>
              <th>Course Name</th>
              <th>Description</th>
              <th>Price</th>
              <th>Slots</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {allCourses}
          </tbody>
        </Table>
      </>
    ) : (
      <Navigate to="/courses" />
    );
}